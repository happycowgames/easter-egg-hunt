﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasterEggSceneScript : MonoBehaviour
{
    [HideInInspector]
    public int TotalEggCount;
    [HideInInspector]
    public int FoundEggCount;
    public string SceneName;

    void Start()
    {
        FoundEggCount = 0;
    }
}
