﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EasterEggHuntScript : MonoBehaviour
{
    private List<EasterEggSceneScript> _easterEggSceneScripts;
    public Text _eggDisplayText;
    private int _currentAreaIndex;
    public GameObject CongratsPanel;
    public ParticleSystem CongratsParticles;

    // Start is called before the first frame update
    void Start()
    {
        _easterEggSceneScripts = new List<EasterEggSceneScript>();
        foreach (var easterEggSceneScript in transform.GetComponentsInChildren<EasterEggSceneScript>()) {
            var hiddenEggScripts = easterEggSceneScript.GetComponentsInChildren<HiddenEggScript>();;
            foreach (var hiddenEggScript in hiddenEggScripts)
            {
                hiddenEggScript.SetClickReporter(this);
            }

            easterEggSceneScript.TotalEggCount = hiddenEggScripts.Length;
            easterEggSceneScript.gameObject.SetActive(false);

            _easterEggSceneScripts.Add(easterEggSceneScript);
        }

        _currentAreaIndex = 0;

        _easterEggSceneScripts[_currentAreaIndex].gameObject.SetActive(true);
        updateEggCounts(0);
    }

    public void ClickedEgg()
    {
        updateEggCounts(_easterEggSceneScripts[_currentAreaIndex].FoundEggCount + 1);

        foreach (var easterEggSceneScript in _easterEggSceneScripts)
        {
            if(easterEggSceneScript.FoundEggCount < easterEggSceneScript.TotalEggCount)
            {
                return;
            }
        }

        if(CongratsPanel != null)
        {
            CongratsPanel.SetActive(true);
        }
        if(CongratsParticles != null)
        {
            CongratsParticles.Play();
        }
    }

    public void ClickedNextButton()
    {
        _easterEggSceneScripts[_currentAreaIndex].gameObject.SetActive(false);
        _currentAreaIndex = (_currentAreaIndex + 1) % _easterEggSceneScripts.Count;
        _easterEggSceneScripts[_currentAreaIndex].gameObject.SetActive(true);
        updateEggCounts(_easterEggSceneScripts[_currentAreaIndex].FoundEggCount);
    }

    private void updateEggCounts(int newFoundCount)
    {
        _easterEggSceneScripts[_currentAreaIndex].FoundEggCount = newFoundCount;
        if (_eggDisplayText == null) { return; }

        _eggDisplayText.text = "Found: " + _easterEggSceneScripts[_currentAreaIndex].FoundEggCount + "/" + _easterEggSceneScripts[_currentAreaIndex].TotalEggCount;
    }
}
