﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenEggScript : MonoBehaviour
{
    private bool _clicked;
    private EasterEggHuntScript _clickReporter;
    private SpriteRenderer _greenCircleOutline;

    public void Start()
    {
        _clicked = false;
        _greenCircleOutline = GetComponentInChildren<SpriteRenderer>();
        _greenCircleOutline.gameObject.SetActive(false);
    }

    public void SetClickReporter(EasterEggHuntScript clickReporter)
    {
        _clickReporter = clickReporter;
    }

    public void OnMouseDown()
    {
        if(_clicked || _clickReporter == null) { return; }

        _clicked = true;
        _clickReporter.ClickedEgg();
        _greenCircleOutline.gameObject.SetActive(true);
    }
}
